from django.contrib import admin
from django.contrib.auth.models import User, Group
from vehicle_dealer_app.models import Vehicle 



admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(Vehicle)