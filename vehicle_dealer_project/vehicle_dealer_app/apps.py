from django.apps import AppConfig


class VehicleDealerAppConfig(AppConfig):
    name = 'vehicle_dealer_app'
