from django.db import models
from vehicle_dealer_app.choices import vehicle_type,engine_type,gearbox_type, year_choices, condition_choices
import datetime

class Vehicle(models.Model):
    vehicle_type =   models.CharField(max_length = 10, choices = vehicle_type)
    make =           models.CharField(max_length=20, default = '')
    model =          models.CharField(max_length=20, default = '' )
    year =           models.IntegerField(choices=year_choices, default=datetime.datetime.now().year)
    price =          models.PositiveIntegerField()
    engine =         models.CharField(max_length = 20, choices=engine_type, default = '' )
    gearbox =        models.CharField(max_length = 20, choices=gearbox_type, default = '')
    mileage =        models.IntegerField()
    condition =      models.CharField(max_length = 10, choices=condition_choices, default = '')
    
    def __str__(self):
        return "%s" % (self.make)

