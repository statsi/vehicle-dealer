from django import forms
from django.forms import ModelForm
from .models import Vehicle

class VehicleForm(ModelForm):
    class Meta:
        model = Vehicle
        fields = ['vehicle_type' , 'make' , 'model' , 'year', 'price' , 'engine' , 'gearbox' , 'mileage' , 'condition']