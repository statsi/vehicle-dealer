import datetime

vehicle_type = (
                ('Cars' , 'Cars'),
                ('Trucks' , 'Trucks'),
                ('Buses' , 'Buses'),
                ('Motors' , 'Motors')
)

year_choices = [(r,r) for r in range(1984, datetime.date.today().year+1)]

engine_type = (
            ('Petrol' , 'Petrol'),
            ('Diesel' , 'Diesel'),
            ('Electric' , 'Electric'),
            ('Hybrid' , 'Hybrid')
)

gearbox_type = (
            ('Manual', 'Manual'),
            ('Automatic' , 'Automatic'),
            ('Semi-Automatic' , 'Semi-Automatic')
)

condition_choices = (
                  ('New', 'New'),
                  ('Used', 'Used')
)




