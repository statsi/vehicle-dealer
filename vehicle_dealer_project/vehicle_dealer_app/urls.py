from django.urls import path
from vehicle_dealer_app import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('',                         views.app_index ,                      name ="index"),
    path('vehiclelist',              views.AllVehicles.as_view(),           name = 'vehicle_list'),
    
    
    path('carlist',                  views.vehicle_list,                    {"veh_type":'Cars'}),
    path('trucklist',                views.vehicle_list,                    {"veh_type":'Trucks'}),
    path('buslist',                  views.vehicle_list,                    {"veh_type":'Buses'}),
    path('motorlist',                views.vehicle_list,                    {"veh_type":'Motors'}),
    
    path('view/<int:pk>',            views.VehicleDetail.as_view(),         name='vehicle_detail'),
    path('edit/<int:pk>',            views.vehicle_update,                  name="vehicle_update"),
    path('delete/<int:pk>',          views.vehicle_delete,                  name="vehicle_confirm_delete"),
    
  
    path('createvehicle/',           views.vehicle_create ,                 name = 'vehicle_create'),
    path('pricesort',                views.SortedByPrice.as_view(),         name = 'sorted_vehicles'),

    path('login/',                   auth_views.login,   {'template_name': './vehicle_dealer_app/login.html'}, name='login'),
    path('logout/',                  auth_views.logout,  {'next_page': '/'},                              name='logout')
      
]       




