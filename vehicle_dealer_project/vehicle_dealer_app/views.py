from .models import Vehicle
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView, DetailView
from .forms import VehicleForm
from django.http import HttpResponseRedirect

# Create your views here.

''' Application Index'''
def app_index(request):
    return render(request,'../templates/index.html')


''' List All Vehicles '''
class AllVehicles(ListView):
    model = Vehicle


''' Details for Vehicle ''' 
class VehicleDetail(DetailView):
    model = Vehicle


''' List Cars, Buses, Trucks, Motors '''     
def vehicle_list(request,  veh_type):
    vehicle = Vehicle.objects.filter(vehicle_type=veh_type)
    data = {}
    data['object_list'] = vehicle
    return render(request, './vehicle_dealer_app/vehicle_list.html', data)


''' Create Vehicle ''' 
def vehicle_create(request):
    if request.user.is_superuser:
        form = VehicleForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('vehicle_list')
        return render(request, './vehicle_dealer_app/vehicle_form.html', {'form':form})


''' Edit Information for Vehicle '''
def vehicle_update(request, pk):
    if request.user.is_superuser:
        vehicle= get_object_or_404(Vehicle, pk=pk)
        form = VehicleForm(request.POST or None,request.FILES, instance=vehicle)
        if form.is_valid():
            form.save()
            return redirect('vehicle_list')
        return render(request, './vehicle_dealer_app/vehicle_form.html', {'form':form})


'''  Delete Vehicle '''
def vehicle_delete(request, pk):
    if request.user.is_superuser:
        vehicle= get_object_or_404(Vehicle, pk=pk)    
        if request.method=='POST':
            vehicle.delete()
            return redirect('vehicle_list')
        return render(request, './vehicle_dealer_app/vehicle_confirm_delete.html' ,{'object':vehicle})


''' Sort Vehicles by Type and Price'''
class SortedByPrice(ListView):
    Model = Vehicle
    template_name = './vehicle_dealer_app/sorted_vehicles.html'
    def get_queryset(self):
        return Vehicle.objects.all().order_by('vehicle_type','price')
